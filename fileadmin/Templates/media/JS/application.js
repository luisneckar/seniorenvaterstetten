$(document).ready(function() {



    $('.contenttable').addClass('table');
    $('.contenttable').addClass('table-responsive');
    $('.contenttable').addClass('table-hover');
    // Cookie
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + "; ";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
        }
        return "";
    }

    var testcookie = getCookie("cookieChecked");
    if(testcookie != "") {
        //$('.cookie').hide();
    } else {
        $('.cookie').removeClass('cookie-hidden');
        $('.js-close-cookie').on("click",function(e){
            e.preventDefault();
            $('.cookie').hide();
            $('.cookie').addClass('cookie-hidden');
            setCookie("cookieChecked", "true");
        });
    }

});

function myFunction() {
    var copyText = document.getElementById("myInput");
    copyText.select();
    copyText.setSelectionRange(0, 99999)
    document.execCommand("copy");
    alert("Copied the text: " + copyText.value);
}
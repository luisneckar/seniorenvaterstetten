lib.contentElement{
	partialRootPaths.100 = EXT:ns_all_lightbox/Resources/Private/Partials/Prettyphoto/TYPO3/
}

page.includeCSS{
	prettyphoto = EXT:ns_all_lightbox/Resources/Public/prettyphoto/css/prettyPhoto.css
}

page.includeJSFooter{
	prettyphoto = EXT:ns_all_lightbox/Resources/Public/prettyphoto/js/jquery.prettyPhoto.js
}

page.footerData {
	1000 = TEXT
	1000.value(
		<script type='text/javascript'>
			$(document).ready(function() {
				$(".prettyphoto").each(function (index) {
				    $(this).attr("rel",$(this).attr('data-gal'));
				});
				$(".prettyphoto").prettyPhoto({
					changepicturecallback: onPictureChanged,
				});

			});
			function onPictureChanged() {
				var twitterDiv = jQuery('.twitter');
				twitterDiv.empty();

				var fbDiv = jQuery('.facebook');
				fbDiv.empty();
			}
		</script>
	)
}
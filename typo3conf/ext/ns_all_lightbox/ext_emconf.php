<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "ns_all_lightbox".
 *
 * Auto generated 24-09-2019 08:35
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => '[NITSAN] All In One Lightbox/Modalbox',
  'description' => 'One of the only TYPO3 extension which provides to use most popular jQuery Lightbox/Modalbox at TYPO3 content elements. This TYPO3 extension provides to configure many jQuery plugins eg., lightbox2, fancybox2, colorbox, prettyphoto, darkbox, magnific-popup & more will be available in an upcoming version.',
  'category' => 'plugin',
  'author' => 'T3:Bhavin Barad, T3:Keval Pandya, QA:Siddharth Sheth',
  'author_email' => 'sanjay@nitsan.in',
  'author_company' => 'NITSAN Technologies Pvt Ltd',
  'state' => 'stable',
  'uploadfolder' => true,
  'createDirs' => '',
  'clearCacheOnLoad' => 0,
  'version' => '3.1.1',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '7.0.0-9.5.99',
      'fluid_styled_content' => '',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
  'clearcacheonload' => true,
);

